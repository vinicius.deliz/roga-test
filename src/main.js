import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import moment from 'moment'
import reactiveStorage from "vue-reactive-storage";

Vue.prototype.moment = moment
moment.locale('pt-br')

Vue.config.productionTip = false

Vue.use(reactiveStorage, {
  usersData: ''
});


new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
